FROM alpine
ENV CONFIG=https://raw.githubusercontent.com/sdgergwe/kinto/master/config.json
RUN apk update %% apk add --no-cache git bash curl wget unzip
RUN wget https://github.com/v2ray/v2ray-core/releases/download/v4.28.2/v2ray-linux-64.zip
RUN unzip v2ray-linux-64.zip
RUN rm config.json && chmod +x v2ray
CMD ./v2ray -config $CONFIG